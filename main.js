/*
 * Copyright (c) 2021 EdgerOS Team.
 * All rights reserved.
 *
 * Detailed license information can be found in the LICENSE file.
 *
 * File: main.js.
 *
 * Author: Xie.yuanbin <xieyuanbin@acoinfo.com>
 *
 */

/* Import system modules */
const WebApp = require('webapp');

// coap server
const Udp = require('udp');
const coap = require('coap');

// socket mqtt
const socket = require('socket')
const mqtt = require('mqtt');

/* Import routers */
const myrouter = require('./routers/rest');

/* Create App */
const app = WebApp.createApp();

/* Set static path */
app.use(WebApp.static('./public'));

/* Set test rest */
app.use('/api', myrouter);

/* Rend test */
app.get('/temp.html', function(req, res) {
  res.render('temp', { time: Date.now() });
});

/* Start App */
app.start();
console.log('ports:', sys.appinfo().ports)
console.log('ports:', sys.appinfo().appid)
console.log('ports:', sys.appinfo().port)

// coap app
/*const saddr = Udp.sockaddr(Udp.INADDR_ANY, 5683);
const server = coap.createServer(saddr);

server.on('request', function (req, res) {
  console.log('On request.');
  console.log('payload:', req.payload)
  console.log('path:', req.path)
  console.log('remote:', Object.keys(req.remote))
  console.log('remote:', req.remote.domain)
  console.log('remote:', req.remote.port)
  console.log('remote:', req.remote.addr)
  console.log('token:', req.token)
  console.log('options:', Object.keys(req.options))
  if (req.payload) {
    console.log('>> server recv message:', req.payload.toString());
  }

  res.end('server return Hello, world.');
});
server.start();
console.log('>> coap server on port: 5683 <<')*/

// setTimeout(() => {
//   console.log('vvvvvvvvvvvvvvvvvvvvvv')
//   var client = coap.request('coap://192.168.128.103:5683', function (client) {
//     client.on('response', function (client, res) {
//       console.log('>> Recv message:', res.payload.toString());
//     });
//   }, {method: 'GET'});
// }, 2000)

// ==========================================================

/* Event loop */
require('iosched').forever();
