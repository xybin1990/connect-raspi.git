/*
 * Copyright (c) 2021 EdgerOS Team.
 * All rights reserved.
 *
 * Detailed license information can be found in the LICENSE file.
 *
 * File: rest.js.
 *
 * Author: hanhui@acoinfo.com
 *
 */

const Router = require('webapp').Router;
const coap = require('coap');

/* Create router */
const router = Router.create();

/* Test call */
router.get('/test', function(req, res) {
  res.send('Hello world!');
});

router.get('/get-distance', function(req, res) {
  // res.send(Math.floor(Math.random() * 100))
  var coapClient = coap.request('coap://192.168.64.89:5683/get-distance', function (client) {
    client.on('response', function (client, data) {
      console.log('>> Recv message:', data.payload.toString());
      res.json({
        code: 0,
        data: data.payload.toString()
      })
    });
  }, {method: 'GET', payload: 'get-distance'});
})

/* Export router */
module.exports = router;
